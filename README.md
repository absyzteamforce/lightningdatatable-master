# LightningDataTable
# Good Practice- Build wrapper Class separatley instead of within Main-controller.

# A new approach to build Lightning DataTable with sortable columns and formatted data as per field type. 

# At the component side, we just have to maintain 2 attributes- ObjectName and Comma separated fields. You can change these attributes any time as per your requirement.

# Now no need to spend hours, we only need 4-5 minutes to build a lightning Data table which is reliable and scalable. 